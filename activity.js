// #3 Insert One Method for Single Room

db.hotelRoom.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": false
})

// #4 Insert Many for Multiple Rooms 
db.hotelRoom.insertMany([
	{
		"name": "double",
		"accomodates": 3,
		"price": 2000,
		"description": "A room fit for a small family going on a vacation",
		"rooms_available": 5,
		"isAvailable": false		
	},
	{
		"name": "queen",
		"accomodates": 4,
		"price": 4000,
		"description": "A room with a queen sized ben perfect for a simple getaway",
		"rooms_available": 15,
		"isAvailable": false
	}
	])

// #5 Find Room to Search for a Double Room
db.hotelRoom.find({
	"name": "double"
})

// #6 Update Method for Queen Room
db.hotelRoom.updateOne(
	{
		"name": "queen",
	},
	{
		$set: {
			"accomodates": 0
		}
	}
	)

// #7 deleteMany for 0 Availability
db.hotelRoom.deleteMany({
	"accomodates": 0
})

// #CORRECTIONS
// Restored deleted room

db.hotelRoom.insertOne({
		"name": "queen",
		"accomodates": 4,
		"price": 4000,
		"description": "A room with a queen sized ben perfect for a simple getaway",
		"rooms_available": 15,
		"isAvailable": false
	})

// #6 CORRECTION
db.hotelRoom.updateOne(
	{
		"name": "queen",
	},
	{
		$set: {
			"rooms_available": 0
		}
	}
	)

// #7 CORRECTION
db.hotelRoom.deleteMany({
	"rooms_available": 0
})